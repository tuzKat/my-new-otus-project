const { I } = inject();

module.exports = {
    head: {
        greetingHead:  '//h2',
    },
    buttons: {
        user: '//span[@class="username"]',
        settings: '//a[@href = "/user/settings"]',
        addList: '[data-icon="plus"]',
        createList: '//button[@class = "button is-success noshadow"]',
        createListDisabled: '//button[@class = "button is-success noshadow"][not(@disabled="disabled")]',
        chooseList: '//span[@class="list-menu-title"]',
        addTask: '//button[@class = "button is-success"]',
    },
    fields: {
        listName: '//input[@placeholder = "The list\'s name goes here..."]',
        taskName: '//input[@placeholder = "Add a new task..."]',
    },
    addedTask: '//span[@class="tasktext"]',

    gotoSettings(){
        I.click(this.buttons.user);
        I.click(this.buttons.settings);
        I.wait(3); //пришлось поставить ожидание, т.к. периодически 502 ошибка отдается
    },

    async getUserGreeting() {
        I.wait(5);
        return greeting = await I.retry(2).grabTextFrom(this.head.greetingHead);
    },

    async addNewList(nameOfList) {
        I.click({ css: this.buttons.addList });
        I.waitForText('Create a new list', 2);
        console.log(`Hi, Kate! I'm trying to add new list with name = ${nameOfList}`);
        I.fillField({ xpath: this.fields.listName }, nameOfList);
        I.click({ xpath: this.buttons.createList });
        I.wait(0.5);
    },

    addNewTask(nameOfList, nameOfTask) {
        I.click({ xpath: this.buttons.chooseList});
        I.waitForText(nameOfList, 3);
        I.fillField({ xpath: this.fields.taskName }, nameOfTask);
        I.click({ xpath: this.buttons.addTask });
    },
}

